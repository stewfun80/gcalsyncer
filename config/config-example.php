<?php
/**
 * CONFIG FILE
 */

return [

    /**
     * CALENDAR ID TO SYNC FOR
     *
     * Call the script with 'listcal:google' as first argument to get the list of available calendars in the google account
     *
     * Fill the values of this array with the calendar id you want to sync.
     * Don't change the keys of the array, to disable a sync leave empty the value.
     *
     * !!! BE VERY CAREFUL, with sync, events in Google Calendar that are not on the external source will be DELETED, !!!
     * !!! so avoid to use an existing calendar and create a different Google Calendar for each enabled source.       !!!
     * !!! Call `php gcalsyncer.php listcal:google` to see which calendars are selected before executing dosync:*.    !!!
     */
    'google_calendars'   => [
        'wordpress' => '',
        'facebook'  => ''
    ],


    /**
     * FACEBOOK iCal feed URL
     * This link can be found somewhere in the Facebook Event Page
     * and it is in this form: https://www.facebook.com/ical/u.php?uid=123456789012345&key=ABCDEFgHijk123ABc
     *
     * If the link you've found start with webcal:// replace it with https://
     *
     * Read more here {@link https://www.facebook.com/help/152652248136178/}
     *
     * !!! For your security, keep this url secret, don't distribute it !!!
     */
    'facebook_ical_feed' => '',


    /**
     * DEFAULT TIMEZONE
     *
     * A valid argument of PHP function date_default_timezone_set
     * {@link http://php.net/manual/function.date-default-timezone-set.php}
     */
    'default_time_zone'  => 'Europe/Rome',


    /**
     * EMAIL CONFIG
     *
     * Configuration to send the report of dosync:* operation via mail
     * used when argument -M|--send-mail is given
     */
    'email_report'       => [

        // string : Transport to use:
        // <NULL>     : NULL or empty to disable mail sending
        // 'SMTP'     : smtp server
        // 'MAIL'     : php mail() function
        // 'QMAIL'
        // 'SENDMAIL'
        'use'                 => 'MAIL',

        // boolean : if true attach to the email some files (for debug purpose)
        'script_debug'        => FALSE,

        // int : verbosity
        'smtp_debug'          => 0,

        // string : SMTP servers, you can specify more than one with ';' as separator
        'smtp_host'           => '',

        // int : SMTP TCP port (NULL for default or no smtp)
        'smtp_port'           => NULL,

        // boolean : If TRUE enable SMTP authentication
        'smtp_auth'           => FALSE,

        // string : SMTP security: 'tls' | 'ssl' | empty
        'smtp_secure'         => '',

        // string : SMTP username (empty to use local ssh user)
        'smtp_username'       => '',

        // string : SMTP password (empty to use local ssh user)
        'smtp_password'       => '',

        // string : SENDER DOMAIN
        'from_domain'         => 'smartattack.net',

        // string : FROM Address 'foo@domain.com' where 'domain.com' is the actual domain (to avoid antispam issues)
        'from_address'        => 'gcalsyncer@smartattack.net',

        // string : FROM humanized name (optional, you can leave empty)
        'from_name'           => 'Google Calendar Syncer',

        // string : REPLY TO Address ex. 'foo@domain.com' leave empty for a no-reply email
        'reply_to_address'    => '',

        // string : REPLY TO humanized name (optional, you can leave empty)
        'reply_to_name'       => '',

        // array : RECIPIENT Addresses who will receive the mail
        //         One or more recipients,
        //         Each recipient is described by an array
        //         1st elem the address es. 'foo.bar@example.com'
        //         2nd (optional) elem the humanized name es. 'Foo Bar'
        //
        'recipient_addresses' => [
            ['', '']
        ],

        // array : CC Addresses, same format as 'recipient_addresses', empty array for no CC
        'cc_addresses'        => [

        ],

        // array : BCC Addresses, same format as 'recipient_addresses', empty array for no BCC
        'bcc_addresses'       => [

        ],
    ]
];