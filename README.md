# Google Calendar Syncer

This script performs a one-way sync from different sources to your Google Calendar(s) using the **Google Calendar API**.

Available sources are:

* Wordpress (in conjunction with plugin ["The Events Calendar"](https://wordpress.org/plugins/the-events-calendar/))
* Facebook

## Usage

You can read the full description in the **help page** after the installation:

```bash
php gcalsyncer.php -h|--help
```

# Installation

## Download from GIT
```bash
git clone https://bitbucket.org/stewfun80/gcalsyncer.git
```

## Install Composer

Install `composer` on your system if not already installed.

Follow the instructions on the composer site: [getcomposer.org](https://getcomposer.org/doc/00-intro.md).

### Composer on Dreamhost

If you are on **Dreamhost**, composer may not be installed and **Phar** extension not enabled,
you can proceed this way:

* locate the local php config folder in your home dir associated with the php version in use,
i.e. if you have PHP 7.2.x the folder is ```~/.php/7.2```
* if the folder doesn't exists, create it
* find or create a file called ```phprc``` in this folder and add the following lines:

```apacheconfig
extension = phar.so 
suhosin.executor.include.whitelist = phar
```

* Save it, now **Phar** should be active, you can check executing ```php -m | grep Phar```

* Now **composer** installation should be simple:

```bash
mkdir ~/bin   # or elsewhere in your home folder
cd ~/bin
curl -sS https://getcomposer.org/installer | php
mv composer.phar composer
```

* Add the ```~/bin``` to your environment ```PATH``` and composer should work!

Useful reads:

* [https://help.dreamhost.com/hc/en-us/articles/214899037-Installing-Composer-overview](https://help.dreamhost.com/hc/en-us/articles/214899037-Installing-Composer-overview) 
* [https://www.geekality.net/2013/02/01/dreamhost-composer/](https://www.geekality.net/2013/02/01/dreamhost-composer/) 

## Install dependencies with composer

Change into the root directory where the script `gcalsyncer.php` is located and run:

```bash
composer install
composer dump-autoload -o
```

## Configuration file

Change into the config dir and copy the config example:

    cd config/
    cp config-example.php config.php

For now it is enough, you'll edit the config file later.

## Authorize the script to use the Google Calendar API

In order to use the script, you need to login into the **Google API Console**, register and download the key that allows the script to use the **Google Calendar API**.

Open this url [https://developers.google.com/calendar/quickstart/php](https://developers.google.com/calendar/quickstart/php) and follow the procedure described in **Step 1**.

Ignore other steps because the Google Client Library was already downloaded by composer.

The generated key on the Google API Console should be saved at this path: ``config/client_secret.json``.

> Keep ``client_secret.json`` private, don't share or publish it to avoid that others use your API-KEY and your quota.

## Authorize the script to manage Google Calendars of a Google Account

Execute:

```bash
php gcalsyncer.php -A check:install
```

and follow the instructions printed on the terminal.

It should tell you that you need to allow the script to manage calendars of a Google Account (OAUTH procedure).

> If the script prints other errors, check the steps up to this point.

At the end of this procedure you should receive a **SUCCESS** message (script is installed successfully).

## Use and configure the script

Now you should be able to retrieve the list of the calendars of the choosen Google Account:

```bash
php gcalsyncer.php -A listcal:google
```

To enable the **sync functionality** read the following configuration steps.

### Enable Facebook Events Sync

* Choose a calendar from the list or create a new one [here](https://calendar.google.com/calendar) **(*****)**
* Edit the file ``config/config.php`` and assign the id of the choosen calendar to the ``'google_calendars'/'facebook'`` directive
* Set the value of the directive ``facebook_ical_feed`` with the url of the **Facebook iCal feed**, you can find this url on the event page of your Facebook (more info are in the config file)

After that you should be able to do the following calls:

```bash
php gcalsyncer.php -A listevents:google-facebook
php gcalsyncer.php -A listevents:facebook

php gcalsyncer.php -A dosync:facebook # !!! READ THE NOTE BELOW (*) !!!
```

### Enable Wordpress Events Sync

* Choose a calendar from the list or create a new one [here](https://calendar.google.com/calendar) **(*****)**
* Edit the file ``config/config.php`` and assign the id of the choosen calendar to the ``'google_calendars'/'wordpress'`` directive
* Download xml-export of events and venues from the **Wordpress Admin Panel**.  
  It works with a valid installation of the plugin ["The Events Calendar"](https://wordpress.org/plugins/the-events-calendar/).  
  Use the **Wordpress Export Tools** that can be found following a link like this: ``https://www.your-wordpress-site.xx/wp-admin/export.php``.  
  Save events into the file ``data/stewfun-wordpress-wp-events.xml`` and venues into ``data/stewfun-wordpress-wp-venues.xml``.  
  _This operation should be done manually every time you want to sync events from wordpress to google or automatized using [wp cli export](https://developer.wordpress.org/cli/commands/export/)._

After that you should be able to do the following calls:

```bash
php gcalsyncer.php -A listevents:google-wordpress
php gcalsyncer.php -A listevents:wordpress

php gcalsyncer.php -A dosync:wordpress # !!! READ THE NOTE BELOW (*) !!!
```

> **(*****) IMPORTANT NOTE**  
> Be very careful, with sync, events in Google Calendar that are not on the external source
> will be **deleted**, so avoid to use an existing calendar and create a different Google Calendar for
> each enabled source.  
> Call ``php gcalsyncer.php -A listcal:google`` to see which calendars are selected
> before executing ``dosync:*``.

### Email logging
If option ``-M|--send-mail`` is given, the log is sent to the configured email instead of to standard error.
This is useful if used with ``cron``. For further explanations, read the help page and ``config-example.php``.

### Optimizations
To avoid waste of Google Calendar API quota and email bombing, in ``dosync:*`` operations the script will neither
call the Google API nor send any email if since the last call there has not been changes from the configured event sources.

# Launchers
In the project root directory are located the ``gcal-sync-*.sh`` scripts that you can use to automatize
the scheduling of the main php script ``gcalsyncer.php`` **after** the initial setup.\
In particular, ``gcal-sync-wps.sh``, first downloads events and venues from wordpress with
``wp``-cli and then launches the google calendar sync.

In order to use theese wrapper scripts, you need to change into ``config/`` folder and copy the ``config-example.sh`` file into ``config.sh``
and edit it (follow the comments inside).\
Don't move the scripts into another folders but feel you free to make **symbolic links** everywhere.
The scripts can be executed from every path.

# Versioning
Current release is *1.0.2 - 2018-06-01*.

Please see [CHANGELOG](CHANGELOG.md) for more information about what has changed recently.

# Author
Stefan Martinelli [stewfun80](https://bitbucket.org/stewfun80/)

# License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
