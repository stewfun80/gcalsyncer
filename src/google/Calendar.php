<?php
/**
 * Dialogs with Google Calendar API
 */

namespace stewfun\google;

use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_CalendarListEntry;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use stewfun\common\Calendar as CommonCalendar;

class Calendar
{

    private $applicationName;
    private $scopes;
    private $params;
    private $client  = NULL;
    private $service = NULL;

    private $internalResult = NULL;


    /**
     * Constructor.
     *
     * @param string $applicationName
     * @param string $scopes
     * @param array  $params
     *
     * @throws \Google_Exception
     */
    public function __construct($applicationName, $scopes, $params = [])
    {
        $this->applicationName = $applicationName;
        $this->scopes          = $scopes;
        $this->params          = $params;

        // Get the API client and construct the service object.
        $this->setClient();
        $this->setService();
    }

    /**
     * @return Google_Service_Calendar
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return Google_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Get the full list of calendar of the account
     *
     * {@link https://developers.google.com/calendar/v3/reference/calendarList/list}
     *
     * @param string $transformer Transformer to apply to format the result or null to get it as is
     *
     * @return Google_Service_Calendar_CalendarListEntry[]
     */
    public function getCalendarList($transformer = null)
    {
        return $this->unpaginate($this->service->calendarList, 'listCalendarList', ['optParams' => []])
            ->applyTransformer($transformer);
    }

    /**
     * Returns all events of the given calendar matching the filter params
     *
     * {@link https://developers.google.com/calendar/v3/reference/events/list}
     *
     * @param string $calendarId  Google calendar id
     * @param array  $optParams   Optional filter params
     * @param string $transformer Transformer to apply to format the result or null to get it as is
     *
     * @return Google_Service_Calendar_Event[] | mixed
     */
    public function getCalendarEventList($calendarId, $optParams = [], $transformer = null)
    {

        return $this->unpaginate($this->service->events, 'listEvents', [
            'calendarId' => $calendarId,
            'optParams'  => $optParams
        ])->applyTransformer($transformer);

    }

    /**
     * Returns the event with the given id of the given calendar
     *
     * {@link https://developers.google.com/calendar/v3/reference/events/get}
     *
     * @param string $calendarId
     * @param string $eventId
     *
     * @return Google_Service_Calendar_Event
     */
    public function getCalendarEvent($calendarId, $eventId)
    {
        return $this->service->events->get($calendarId, $eventId);
    }

    /**
     * Insert a new event into the given calendar
     *
     * {@link https://developers.google.com/calendar/v3/reference/events/insert}
     *
     * @param string $calendarId Google calendar id
     * @param array  $eventData
     *
     * @return Google_Service_Calendar_Event
     */
    public function insertCalendarEvent($calendarId, $eventData)
    {
        return $this->service->events->insert($calendarId, new Google_Service_Calendar_Event($eventData));
    }

    /**
     * Import a new event into the given calendar
     *
     * {@link https://developers.google.com/calendar/v3/reference/events/import}
     *
     * @param string $calendarId Google calendar id
     * @param array  $eventData
     *
     * @return Google_Service_Calendar_Event
     */
    public function importCalendarEvent($calendarId, $eventData)
    {

        $event = new Google_Service_Calendar_Event();

        if (isset($eventData['iCalUID'])) {
            $event->setICalUID($eventData['iCalUID']);
        }

        if (isset($eventData['status'])) {
            $event->setStatus($eventData['status']);
        }

        if (isset($eventData['summary'])) {
            $event->setSummary($eventData['summary']);
        }

        if (isset($eventData['location'])) {
            $event->setLocation($eventData['location']);
        }

        if (isset($eventData['start'])) {
            $startTs = new Google_Service_Calendar_EventDateTime();
            if (isset($eventData['start']['date'])) {
                $startTs->setDate($eventData['start']['date']);
            }
            if (isset($eventData['start']['dateTime'])) {
                $startTs->setDateTime($eventData['start']['dateTime']);
            }
            if (isset($eventData['start']['timeZone'])) {
                $startTs->setTimeZone($eventData['start']['timeZone']);
            }

            $event->setStart($startTs);
        }

        if (isset($eventData['end'])) {
            $endTs = new Google_Service_Calendar_EventDateTime();
            if (isset($eventData['end']['date'])) {
                $endTs->setDate($eventData['end']['date']);
            }
            if (isset($eventData['end']['dateTime'])) {
                $endTs->setDateTime($eventData['end']['dateTime']);
            }
            if (isset($eventData['end']['timeZone'])) {
                $endTs->setTimeZone($eventData['end']['timeZone']);
            }

            $event->setEnd($endTs);
        }

        if (isset($eventData['description'])) {
            $event->setDescription($eventData['description']);
        }

        return $this->service->events->import($calendarId, $event);
    }

    /**
     * Updates an event with the given id of the given calendar
     *
     * {@link https://developers.google.com/calendar/v3/reference/events/update}
     *
     * @param string $calendarId
     * @param string $eventId
     * @param array  $eventData
     *
     * @return Google_Service_Calendar_Event
     */
    public function updateCalendarEvent($calendarId, $eventId, $eventData)
    {

        $event = $this->getCalendarEvent($calendarId, $eventId);

        if (isset($eventData['iCalUID'])) {
            $event->setICalUID($eventData['iCalUID']);
        }

        if (isset($eventData['status'])) {
            $event->setStatus($eventData['status']);
        }

        if (isset($eventData['summary'])) {
            $event->setSummary($eventData['summary']);
        }

        if (isset($eventData['location'])) {
            $event->setLocation($eventData['location']);
        }

        if (isset($eventData['start'])) {
            $startTs = new Google_Service_Calendar_EventDateTime();
            if (isset($eventData['start']['date'])) {
                $startTs->setDate($eventData['start']['date']);
            }
            if (isset($eventData['start']['dateTime'])) {
                $startTs->setDateTime($eventData['start']['dateTime']);
            }
            if (isset($eventData['start']['timeZone'])) {
                $startTs->setTimeZone($eventData['start']['timeZone']);
            }

            $event->setStart($startTs);
        }

        if (isset($eventData['end'])) {
            $endTs = new Google_Service_Calendar_EventDateTime();
            if (isset($eventData['end']['date'])) {
                $endTs->setDate($eventData['end']['date']);
            }
            if (isset($eventData['end']['dateTime'])) {
                $endTs->setDateTime($eventData['end']['dateTime']);
            }
            if (isset($eventData['end']['timeZone'])) {
                $endTs->setTimeZone($eventData['end']['timeZone']);
            }

            $event->setEnd($endTs);
        }

        if (isset($eventData['description'])) {
            $event->setDescription($eventData['description']);
        }

        return $this->service->events->update($calendarId, $event->getId(), $event);
    }

    /**
     * Returns an authorized API client.
     *
     * @return Google_Client the authorized client object
     * @throws \Google_Exception
     */
    private function setClient()
    {
        $client = new Google_Client();
        $client->setApplicationName($this->applicationName);
        $client->setScopes($this->scopes);
        $client->setAuthConfig($this->params['client_secret']);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->expandHomeDirectory($this->params['credentials']);
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n" .
                "| | | | | | | | | | | | | | | | | | | | |\n" .
                "v v v v v v v v v v v v v v v v v v v v v\n" .
                "%s\n\n", $authUrl);
            print 'AFTER you have completed the OAUTH procedure on your browser, enter the verification code here: ';
            $authCode = trim(fgets(STDIN));

            print "\n";

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if (!file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }

        $this->client = $client;

        return $this->client;
    }

    /**
     * Instantiate the service calendar
     *
     * @return Google_Service_Calendar
     */
    private function setService()
    {
        $this->service = new Google_Service_Calendar($this->getClient());

        return $this->service;
    }

    /**
     * Expands the home directory alias '~' to the full path.
     *
     * @param string $path the path to expand.
     *
     * @return string the expanded path.
     */
    private function expandHomeDirectory($path)
    {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
            $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }

    /**
     * Saves internally an array of response objects given by a call to a google service
     *
     * Unpaginate the result (calling more times if needed) of $object->$method call.
     * $object->$method should belongs to a valid google service request that responds
     * with a paginated result.
     * $params is the array of params, should be an associative array, params should
     * be declared in the order expected by the method $object->$method.
     * Be sure to define in the $params array the key 'optParams', the relative value
     * must be an array, if there are not optparams, set the value to empty array.
     * optparams is used to handle pagination.
     * The result is stored internally and the method returns the instance itself.
     *
     * @param       $object
     * @param       $method
     * @param array $params
     *
     * @return Calendar
     */
    private function unpaginate($object, $method, $params = [])
    {
        $entries = [];

        do {
            $list = call_user_func_array(array($object, $method), $params);

            foreach ($list->getItems() as $listEntry) {
                $entries[] = $listEntry;
            }

            $pageToken = $list->getNextPageToken();
            if ($pageToken) {
                $params['optParams']['pageToken'] = $pageToken;
            } else {
                break;
            }
        } while (true);

        $this->internalResult = $entries;

        return $this;
    }

    /**
     * Returns the transformed version of internalResult applying the given transformer
     *
     * @param string|null $transformer method suffix or null
     * @param array       $transformerParams
     *
     * @return mixed
     */
    private function applyTransformer($transformer = null, $transformerParams = [])
    {

        // No transformation
        if (is_null($transformer)) {
            return $this->internalResult;
        }

        return call_user_func_array(array($this, 'transform' . $transformer), $transformerParams);
    }

    /**
     * Transform the internalResult from CalendarEventList[] to an associative array with iCalUID as primary key
     *
     * @return array
     */
    private function transformCalendarEventListToArrayByICalUID()
    {
        if (!is_array($this->internalResult) || empty($this->internalResult)) {
            return [];
        }

        $result = [];

        $i = 0;
        foreach ($this->internalResult as $item) {
            $pkey = $item->getICalUID();
            if (trim($pkey) == '') {
                $pkey = '__not_set_' . $i . '_' . md5(microtime());
            }

            $itemAssoc = [
                'id'             => $item->getId(),
                'iCalUID'        => $item->getICalUID(),
                'hash'           => NULL,
                'status'         => $item->getStatus(),
                'html_link'      => $item->getHtmlLink(),
                'summary'        => $item->getSummary(),
                'location'       => $item->getLocation(),
                'start_ts'       => trim($item->getStart()->getDateTime()) != '' ? $item->getStart()->getDateTime() : $item->getStart()->getDate(),
                'start_timezone' => $item->getStart()->getTimeZone(),
                'end_ts'         => trim($item->getEnd()->getDateTime()) != '' ? $item->getEnd()->getDateTime() : $item->getEnd()->getDate(),
                'end_timezone'   => $item->getEnd()->getTimeZone(),
                'description'    => $item->getDescription(),
                'created'        => $item->getCreated(),
                'updated'        => $item->getUpdated(),
            ];

            $itemAssoc['hash'] = CommonCalendar::calcEventHash($itemAssoc);

            $result[$pkey] = $itemAssoc;

            $i++;

        }

        return $result;
    }
}
