<?php
/**
 * Parsing of events ICAL export from facebook
 */

namespace stewfun\facebook;

use Exception;
use Curl\Curl;
use Cocur\Slugify\Slugify;
use ICal\ICal;
use stewfun\common\Calendar as CommonCalendar;

class EventICAL
{

    private $iCal             = null;
    private $iCalLocalPath    = null;
    private $options          = [];
    private $sourceHasChanged = false;

    private $fileSources = [];


    /**
     * Constructor.
     *
     * @param string $iCalFeedUrl   Url of the iCal feed
     * @param string $iCalLocalPath Local path where to save the downloaded ics feed
     * @param array  $iCalOptParams Optional param for the iCal instance
     * @param array  $options       Class options
     *
     * @throws \ErrorException
     */
    public function __construct($iCalFeedUrl, $iCalLocalPath, $iCalOptParams = [], $options = [])
    {
        $this->options       = $options;
        $this->iCalLocalPath = $iCalLocalPath;
        $this->downloadICal($iCalFeedUrl, $iCalLocalPath);
        $this->iCal = $this->parseICal($iCalLocalPath, $iCalOptParams);
    }

    /**
     * Returns the used file sources
     *
     * @return array
     */
    public function getFileSources()
    {
        return $this->fileSources;
    }

    /**
     * Returns TRUE if the source has changed since last check
     *
     * @return bool
     */
    public function getSourceHasChanged()
    {
        return $this->checkSourcesHaveChanged();
    }

    /**
     * Returns an associative array of events
     *
     * @return array
     * @throws Exception
     */
    public function fetchAllAssoc()
    {

        //
        // EVENTS
        //
        $min_start_ts   = null;
        $max_end_ts     = null;
        $statuses       = [];
        $total_events   = 0;
        $eventsAllAssoc = [];
        foreach ($this->iCal->events() as $item) {

            $update_date = null;
            if (trim($item->last_modified) != '') {
                $update_date = CommonCalendar::timeToIso8601($item->last_modified, 'UTC', $this->options['timeZone']);
            } elseif (trim($item->lastmodified) != '') {
                $update_date = CommonCalendar::timeToIso8601($item->last_modified, 'UTC', $this->options['timeZone']);
            }

            if (trim($item->dtstart) != '') {
                $start_date = CommonCalendar::timeToIso8601($item->dtstart, 'UTC', $this->options['timeZone']);
            } else {
                $start_date = CommonCalendar::timeToIso8601($item->dtstart_tz, 'UTC', $this->options['timeZone']);
            }

            if (trim($item->dtend) != '') {
                $end_date = CommonCalendar::timeToIso8601($item->dtend, 'UTC', $this->options['timeZone']);
            } else {
                $end_date = CommonCalendar::timeToIso8601($item->dtend_tz, 'UTC', $this->options['timeZone']);
            }

            $eventAssoc = [
                'id'              => (string)$item->uid,
                'class'           => (string)$item->class,
                'status'          => (string)$item->status,
                'partecip_status' => (string)$item->partstat,
                'create_date'     => CommonCalendar::timeToIso8601($item->created, 'UTC', $this->options['timeZone']),
                'update_date'     => $update_date,
                'stamp_date'      => CommonCalendar::timeToIso8601($item->dtstamp, 'UTC', $this->options['timeZone']),
                'start_ts'        => $start_date,
                'end_ts'          => $end_date,
                'timezone'        => $this->options['timeZone'],
                'location'        => (string)$item->location,
                'title'           => (string)$item->summary,
                'url'             => (string)$item->url,
                'description'     => (string)$item->description,
            ];

            // Last formatting
            $eventAssoc = $this->eventFormat($eventAssoc);

            // Saving event
            $eventsAllAssoc[$eventAssoc['id']] = $eventAssoc;

            // Metadata
            if (is_null($min_start_ts) ||
                (!empty($eventAssoc['start_ts']) && $eventAssoc['start_ts'] < $min_start_ts)) {
                $min_start_ts = $eventAssoc['start_ts'];
            }
            if (is_null($max_end_ts) ||
                (!empty($eventAssoc['end_ts']) && $eventAssoc['end_ts'] > $max_end_ts)) {
                $max_end_ts = $eventAssoc['end_ts'];
            }
            if (!isset($statuses[$eventAssoc['status']])) {
                $statuses[$eventAssoc['status']] = 1;
            } else {
                $statuses[$eventAssoc['status']]++;
            }
            $total_events++;
        }


        //
        // Merge with metadata
        //
        $eventDataSet = [
            'meta'   => [
                'min_start_ts' => $min_start_ts,
                'max_end_ts'   => $max_end_ts,
                'statuses'     => $statuses,
                'total_events' => $total_events,

            ],
            'events' => $eventsAllAssoc
        ];

        return $eventDataSet;
    }


    /**
     * Downloads using cUrl the iCal feed and saves it locally as ics file
     *
     * @param string $iCalFeedUrl   Url of the iCal feed
     * @param string $iCalLocalPath Local path where to save the downloaded ics feed
     *
     * @return bool
     * @throws \ErrorException
     */
    private function downloadICal($iCalFeedUrl, $iCalLocalPath)
    {

        // Renames the actual file into old
        $iCalLocalPathPathInfo = pathinfo($iCalLocalPath);
        $iCalLocalPathOld      =
            $iCalLocalPathPathInfo['dirname'] . '/' . $iCalLocalPathPathInfo['filename'] . '-old.' .
            $iCalLocalPathPathInfo['extension'];
        if (file_exists($iCalLocalPath)) {
            rename($iCalLocalPath, $iCalLocalPathOld);
        }

        $this->fileSources = [$iCalLocalPath, $iCalLocalPathOld];

        // Download via Curl the new ical feed
        $curl = new Curl();
        $curl->setOpt(CURLOPT_ENCODING, '');
        //$curl->setHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        $curl->setHeader('Accept', '*/*');
        //$curl->setHeader('Accept-Encoding', 'gzip, deflate, br');
        $curl->setHeader('Accept-Language', 'en-US,en;q=0.5');
        $curl->setHeader('Connection', 'keep-alive');
        $curl->setHeader('Host', 'www.facebook.com');
        $curl->setHeader('Upgrade-Insecure-Requests', '1');
        //$curl->setOpt(CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1');
        //$curl->setOpt(CURLOPT_USERAGENT, 'PostmanRuntime/7.26.5');
        $curl->setOpt(CURLOPT_USERAGENT, 'GCalSyncerRuntime/2020.10.18');
        $curl->download($iCalFeedUrl, $iCalLocalPath);
        if ($curl->error) {
            throw new Exception('CURL Error in downloading iCal feed: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        }
        if (!file_exists($iCalLocalPath) || filesize($iCalLocalPath) < 1) {
            throw new Exception('iCal feed downloaded via curl is empty');
        }
        // Check if is a valid V-Calendar file
        $iCalData = file($iCalLocalPath);
        if (strtoupper(trim($iCalData[0])) != 'BEGIN:VCALENDAR' ||
            strtoupper(trim($iCalData[count($iCalData) - 1])) != 'END:VCALENDAR') {
            throw new Exception('iCal feed downloaded via curl is not a valid ICalendar format https://en.wikipedia.org/wiki/ICalendar');
        }

        $curl->close();

        return true;
    }

    /**
     * Checks, set and returns if datasources have changed since last check or not
     *
     * @return bool
     */
    private function checkSourcesHaveChanged()
    {
        // File in which hashes are stored
        $srcHashesFile = dirname($this->iCalLocalPath) . "/stewfun-facebook-fb-hashes.txt";

        // Calculates actual hashes
        $fbICalHash = $this->getDataSourceHash($this->iCalLocalPath);

        // Hashes file not found, we mark the sources changed since last time
        if (!is_file($srcHashesFile)) {
            $this->sourceHasChanged = true;
        } // Hashes file found, we read it
        else {
            $lastHashes = explode("|", trim(file_get_contents($srcHashesFile)));

            if (is_null($fbICalHash) || !is_array($lastHashes) ||
                !isset($lastHashes[0]) || trim($lastHashes[0]) == '' ||
                $lastHashes[0] != $fbICalHash) {
                $this->sourceHasChanged = true;
            } else {
                $this->sourceHasChanged = false;
            }
        }

        // If source is changed since last time we write/update the hashes file
        if ($this->sourceHasChanged == true) {
            file_put_contents($srcHashesFile, implode('|', [$fbICalHash]));
        }

        return $this->sourceHasChanged;
    }

    /**
     * Returns the hash of the file representing the data-source, NULL if not present
     *
     * @param string $dataSourceFile Path to the source saved locally as a file
     *
     * @return null|string
     */
    private function getDataSourceHash($dataSourceFile)
    {
        if (!file_exists($dataSourceFile) || !is_file($dataSourceFile) || !is_readable($dataSourceFile)) {
            return NULL;
        }

        return hash_file('md5', $dataSourceFile);
    }

    /**
     * Reads the iCal file/url through ICal class and return an ICal instance
     *
     * If the read fails throws exception
     *
     * @param string $iCalSource Ical file path or url
     * @param array  $optParams  Optional parameter
     *
     * @return ICal
     */
    private function parseICal($iCalSource, $optParams = [])
    {
        return new ICal($iCalSource, $optParams);
    }

    /**
     * Format event associative to the format used in event comparison
     *
     * @param array $item Associative array that describes an event
     *
     * @return array
     */
    private function eventFormat($item)
    {

        $slugify = new Slugify();

        $itemFormatted = [
            'id'             => $item['id'],
            'hash'           => null,
            'status'         => $item['status'] . ':' . $item['partecip_status'] . ':' . $item['class'],
            'slug'           => $slugify->slugify($item['title']),
            'html_link'      => $item['url'],
            'summary'        => strip_tags(html_entity_decode($item['title'], ENT_QUOTES)),
            'location'       => $item['location'],
            'start_ts'       => $item['start_ts'],
            'start_timezone' => $item['timezone'],
            'end_ts'         => $item['end_ts'],
            'end_timezone'   => $item['timezone'],
            'description'    => null,
            'created'        => $item['create_date'],
            'updated'        => $item['update_date'],
        ];

        $description = <<<EOD
<b>#title#</b>
<b><i>#status#</i></b>
<a href="#link#"><b>[Scheda evento]</b></a>

<b><u>Luogo:</u></b>
#location#

#description#
EOD;

        $partecipStatusHuman   = '';
        $partecipStatusSummary = '';
        switch ($item['partecip_status']) {
            case 'ACCEPTED' :
                $partecipStatusSummary = '[P]';
                $partecipStatusHuman   = 'Parteciper&ograve; ' . $partecipStatusSummary;
                break;
            case 'TENTATIVE' :
                $partecipStatusSummary = '[I]';
                $partecipStatusHuman   = 'Mi interessa ' . $partecipStatusSummary;
                break;
            case 'NEEDS-ACTION' :
                $partecipStatusSummary = '[?]';
                $partecipStatusHuman   = 'Invitato ' . $partecipStatusSummary;
                break;
            default:
                $partecipStatusSummary = '';
                $partecipStatusHuman   = '';
        }

        $itemFormatted['description'] = strtr($description, [
            '#title#'       => $item['title'],
            '#status#'      => $partecipStatusHuman,
            '#link#'        => $item['url'],
            '#location#'    => $item['location'],
            '#description#' => $item['description'],
        ]);

        $itemFormatted['summary'] = $partecipStatusSummary . ' ' . $itemFormatted['summary'];

        $itemFormatted['hash'] = CommonCalendar::calcEventHash($itemFormatted);

        return $itemFormatted;
    }
}