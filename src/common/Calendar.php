<?php
/**
 * Common and generic methods related to calendar and events
 */

namespace stewfun\common;

use DateTime;
use DateTimeZone;
use Exception;

class Calendar
{

    /**
     * Convert every string timestamp recognized by the strtotime function to ISO8601 formatted string
     *
     * If the strtotime fails, returns null
     *
     * @param string $timestamp
     * @param string $timeZoneSource Optional timezone to convert from
     * @param string $timeZoneTarget Optional timezone to convert to
     *
     * @return string|null
     */
    static public function timeToIso8601($timestamp, $timeZoneSource = null, $timeZoneTarget = null)
    {

        if (trim($timestamp) == '') {
            return NULL;
        }

        if (empty($timeZoneSource)) {
            $timeZoneSource = Control::$CONF['default_time_zone'];
        }
        if (empty($timeZoneTarget)) {
            $timeZoneTarget = Control::$CONF['default_time_zone'];
        }


        $matches = [];
        if (preg_match('/UTC([0-9\+\-\:]+)/', $timeZoneSource, $matches)) {
            $timeZoneSource = $matches[1];
        }

        $matches = [];
        if (preg_match('/UTC([0-9\+\-\:]+)/', $timeZoneTarget, $matches)) {
            $timeZoneTarget = $matches[1];
        }

        try {
            $dt = new DateTime($timestamp, new DateTimeZone($timeZoneSource));
            $dt->setTimezone(new DateTimeZone($timeZoneTarget));
        } catch (Exception $e) {
            return null;
        }

        return $dt->format("c");

    }

    /**
     * If timezone_id is a valid IANA Time Zone Identifier name e.g. "Europe/Zurich" returns is, otherwise default
     * timezone in config
     *
     * @param string $timezone_id
     *
     * @return string
     *
     * @todo: autodetect using abbreviations and offsets defined in this array: DateTimeZone::listAbbreviations()
     */
    static public function sanitizeTimezoneIdentifier($timezone_id)
    {
        if (!in_array($timezone_id, DateTimeZone::listIdentifiers())) {
            return Control::$CONF['default_time_zone'];
        }

        return $timezone_id;
    }

    /**
     * Generate the hash of an event
     *
     * Build hash by calculating the md5 of some fields to easily detect event's changes
     *
     * @param array $item Array with event fields
     *
     * @return string
     */
    static public function calcEventHash($item)
    {

        $preHash = '';
        $preHash .= '|' . ($item['summary'] ?? 'summary');
        $preHash .= '|' . ($item['location'] ?? 'location');
        $preHash .= '|' . ($item['start_ts'] ?? 'start_ts');
        $preHash .= '|' . ($item['start_timezone'] ?? 'start_timezone');
        $preHash .= '|' . ($item['end_ts'] ?? 'end_ts');
        $preHash .= '|' . ($item['end_timezone'] ?? 'end_timezone');
        $preHash .= '|' . ($item['description'] ?? 'description');

        return md5($preHash);
    }
}