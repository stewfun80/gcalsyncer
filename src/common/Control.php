<?php
/**
 * Script control handler
 */

namespace stewfun\common;

use Cocur\Slugify\Slugify;
use Exception;
use Html2Text\Html2Text;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;

class Control
{
    static public $CONF;
    static public $DIR;
    static public $OPTS;
    static public $action;
    static public $actionParts;

    static public $lastError;

    static private $logFile    = null;
    static private $logFileOld = null;

    /**
     * Send the report using the choosed transport
     *
     * Print the message to standard error or to a log file, depending of option -M|--send-mail.
     * If $exit_code is set, the script is stopped sending the passed $exit_code.
     * If it's requested to send error log via mail, messages are appended to the error log
     * file and when $exitCode is set, the mail will be sent with the error log file as attachment,
     * the file is renamed and the script stopped.
     *
     * @param string $message        Message
     * @param int    $exitCode       If not null the script stops and $exit_code is returned
     * @param array  $counters       Used to write a human readable report of what is changed (only with EXIT_OK and
     *                               email enabled)
     * @param array  $addAttachments Array of additional attachments that will be sent if in debug mode (list of full
     *                               path names)
     *
     * @return void
     */
    static public function sendMessage($message, $exitCode = NULL, $counters = [], $addAttachments = [])
    {

        // If logFile is not yet initialized choose where to log
        if (is_null(self::$logFile)) {
            self::$logFile    = 'php://stderr';
            self::$logFileOld = null;
            if (isset(self::$OPTS['M']) || isset(self::$OPTS['send-mail'])) {
                $slugify                 = new Slugify();
                $action_dosync_slugified = $slugify->slugify(self::$action, 7);

                $ext              = (self::$actionParts[0] == 'dosync' ? 'csv' : 'txt');
                self::$logFile    = self::$DIR . '/data/gcalsyncer_' . $action_dosync_slugified . '_log.' . $ext;
                self::$logFileOld = self::$DIR . '/data/gcalsyncer_' . $action_dosync_slugified . '_lastlog.' . $ext;
            }
        }

        // Write the message
        file_put_contents(self::$logFile, $message, FILE_APPEND);

        // Stops the script
        if (!is_null($exitCode)) {

            // Log should be sent via email
            if (!is_null(self::$logFile) && self::$logFile != 'php://stderr') {

                // PHPMailer instance - passing true enables exceptions
                $mail = new PHPMailer(true);

                try {
                    // Set PHPMailer to work with UTF-8 charset
                    $mail->CharSet = 'utf-8';

                    // Message Id
                    $mail->MessageID =
                        "<" . md5('STEWFUN-GCALSYNCER' . (idate("U") - 1000000000) . uniqid()) .
                        '@' . self::$CONF['email_report']['from_domain'] . '>';

                    // Server settings
                    $mail->SMTPDebug = self::$CONF['email_report']['smtp_debug'] ?? 0;

                    switch (self::$CONF['email_report']['use']) {
                        case 'SMTP' :
                            $mail->isSMTP();
                            break;
                        case 'MAIL' :
                            $mail->isMail();
                            break;
                        case 'QMAIL' :
                            $mail->isQmail();
                            break;
                        case 'SENDMAIL' :
                            $mail->isSendmail();
                            break;
                    }

                    if (!empty(self::$CONF['email_report']['smtp_host'])) {
                        $mail->Host = self::$CONF['email_report']['smtp_host'];
                    }

                    if (!empty(self::$CONF['email_report']['smtp_port'])) {
                        $mail->Port = self::$CONF['email_report']['smtp_port'];
                    }

                    $mail->SMTPAuth = self::$CONF['email_report']['smtp_auth'];

                    if (!empty(self::$CONF['email_report']['smtp_username']) &&
                        !empty(self::$CONF['email_report']['smtp_password'])) {
                        $mail->Username = self::$CONF['email_report']['smtp_username'];
                        $mail->Password = self::$CONF['email_report']['smtp_password'];
                    }

                    $mail->SMTPSecure = self::$CONF['email_report']['smtp_secure'];

                    // Recipients
                    $mail->setFrom(self::$CONF['email_report']['from_address'],
                        self::$CONF['email_report']['from_name'], FALSE);

                    if (!empty(self::$CONF['email_report']['reply_to_address'])) {
                        $mail->addReplyTo(self::$CONF['email_report']['reply_to_address'],
                            self::$CONF['email_report']['reply_to_name']);
                    }

                    foreach (self::$CONF['email_report']['recipient_addresses'] as $rec) {
                        if (count($rec) == 1) {
                            $mail->addAddress($rec[0]);
                        } elseif (count($rec) > 1) {
                            $mail->addAddress($rec[0], $rec[1]);
                        }
                    }

                    foreach (self::$CONF['email_report']['cc_addresses'] as $rec) {
                        if (count($rec) == 1) {
                            $mail->addCC($rec[0]);
                        } elseif (count($rec) > 1) {
                            $mail->addCC($rec[0], $rec[1]);
                        }
                    }

                    foreach (self::$CONF['email_report']['bcc_addresses'] as $rec) {
                        if (count($rec) == 1) {
                            $mail->addBCC($rec[0]);
                        } elseif (count($rec) > 1) {
                            $mail->addBCC($rec[0], $rec[1]);
                        }
                    }

                    // Attachment for debugging
                    if (isset(self::$CONF['email_report']['script_debug']) &&
                        self::$CONF['email_report']['script_debug'] == true) {

                        $mail->addAttachment(self::$logFile, basename(self::$logFile));
                        foreach ($addAttachments as $atc) {
                            $mail->addAttachment($atc, basename($atc));
                        }
                    }

                    // Content
                    $mail->isHTML(true);

                    // Subject and body
                    $subjectAction = self::$action;
                    if (self::$actionParts[0] == 'dosync') {
                        $subjectAction = ucfirst(strtolower(self::$actionParts[1]));
                    }
                    if ($exitCode != EXIT_OK) {
                        $mail->Subject = $subjectAction . ' ERROR';
                        $mail->Body    = '<b>Google Calendar Syncer failed</b>, see the attached logfile.';
                        $mail->AltBody = 'Google Calendar Syncer failed, see the attached logfile.';
                    } else {
                        $mail->Subject = $subjectAction . ' UPDATED';
                        $mail->Body    = self::getHumanDescriptionFromCounters($counters);
                        $mail->AltBody = (new Html2Text($mail->Body, ['width' => 1000]))->getText();
                    }

                    // Charset normalization
                    $htmlEncoding = mb_detect_encoding($mail->Body);
                    $textEncoding = mb_detect_encoding($mail->AltBody);
                    if (strtolower($htmlEncoding) != 'utf-8') {
                        $mail->Body = mb_convert_encoding($mail->Body, 'UTF-8', $htmlEncoding);
                    }
                    if (strtolower($textEncoding) != 'utf-8') {
                        $mail->AltBody = mb_convert_encoding($mail->AltBody, 'UTF-8', $textEncoding);
                    }

                    $mail->send();
                } catch (PHPMailerException $e) {
                    self::$lastError = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
                    file_put_contents('php://stderr', "WARNING! " . self::$lastError . "\n");
                } catch (Exception $e) {
                    self::$lastError = 'Generic exception thrown: ' . $e->getMessage();
                    file_put_contents('php://stderr', "WARNING! " . self::$lastError . "\n");
                }

                // Renames the logfile
                rename(self::$logFile, self::$logFileOld);
            }

            exit($exitCode);
        }
    }

    /**
     * Returns the html descriptions of events' updates reading the counters
     *
     * @param array $counters
     *
     * @return string
     */
    static private function getHumanDescriptionFromCounters($counters)
    {

        $trackedKeys = [
            'inserted_events_list'       => ['1 new event created', '%d new events created'],
            'updated_events_list'        => ['1 event updated', '%d events updated'],
            'resumed_events_list'        => ['1 event resumed', '%d events resumed'],
            'soft_cancelled_events_list' => ['1 event deleted', '%d events deleted'],
            'same_events_list'           => ['1 event unchanged', '%d events unchanged'],
        ];

        $htmlBody = '';

        $htmlBody .= '<b>Google Calendar Syncer executed</b><br /><br />';

        foreach ($trackedKeys as $k => $v) {
            if (isset($counters[$k]) && !empty($counters[$k])) {

                // Sorting events for start_ts DESC, summary ASC
                usort($counters[$k], function ($a, $b) {
                    // Descending for 'start_ts' - most recent first
                    $retval = $a['start_ts'] <=> $b['start_ts'];
                    $retval *= -1;
                    // Ascending for summary
                    if ($retval == 0) {
                        $retval = $a['summary'] <=> $b['summary'];
                    }
                    return $retval;
                });

                $evtCnt = count($counters[$k]);
                if ($evtCnt == 1) {
                    $htmlBody .= '<b><u>' . $v[0] . '</u>:</b><br />';
                } else {
                    $htmlBody .= sprintf('<b><u>' . $v[1] . '</u>:</b><br />', $evtCnt);
                }

                $htmlBody .= '<ul>';
                foreach ($counters[$k] as $evt) {
                    $htmlBody .= sprintf(
                        '<li style="%s">%s <b>%s</b>: [%s | %s]</li>',

                        strtotime($evt['start_ts']) < time() ? 'font-style: italic; color:#777777;' : '',
                        date('d/m/Y H:i', strtotime($evt['start_ts'])),
                        $evt['summary'],
                        trim($evt['remote_html_link']) != '' ? '<a href="' . $evt['remote_html_link'] . '">SRC</a>' : '',
                        trim($evt['google_html_link']) != '' ? '<a href="' . $evt['google_html_link'] . '">GCAL</a>' : ''
                    );
                }
                $htmlBody .= '</ul><br />';
            }
        }

        $htmlBody .= '<b><i>Details in the attached logfile</i></b>';

        return $htmlBody;
    }
}