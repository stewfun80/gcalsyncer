<?php
/**
 * GOOGLE CALENDAR SYNCER
 *
 * One-way sync from different event sources to Google Calendar(s)
 *
 * Usage: php gcalsyncer.php <action>
 *
 * DOCUMENTATION
 * -------------
 * For install and configuring: README.md
 *
 * For use: php gcalsyncer.php -h|--help
 *
 * @version 1.0.2-2018-06-01
 * @author  [stewfun80](https://bitbucket.org/stewfun80/)
 *
 * @todo    : use a prefix in iCalUid to distinguish between different sources and allow to use the same GoogleCalendar
 *          for more sources
 * @todo    : allow the usage of more Google Accounts (giving to each one a label, more credentials.json,
 *          account-switch command)
 */

namespace stewfun;

/**
 * VERSIONING
 */
define('SCRIPT_VERSION_SEMVER', '1.0.2');
define('SCRIPT_VERSION_DATE', '2018-06-01');

/**
 * EXIT ERROR CODES
 */
define('EXIT_OK', 0);
define('EXIT_ERR_BAD_INSTALL', 1);
define('EXIT_ERR_BAD_CONFIGURATION', 2);
define('EXIT_ERR_BAD_PARAMS', 3);
define('EXIT_ERR_XML_INIT', 10);
define('EXIT_ERR_GOOGLE_INIT', 20);
define('EXIT_ERR_GOOGLE_GET_CAL_EVENTS', 21);
define('EXIT_ERR_GOOGLE_INSERT_CAL_EVENT', 22);
define('EXIT_ERR_GOOGLE_UPDATE_CAL_EVENT', 23);
define('EXIT_ERR_ICAL_INIT', 30);


/**
 * Perform initial install check
 */
__checkInstall('BASIC');


/**
 * AUTOLOAD AND NAMESPACES
 */
require __DIR__ . '/vendor/autoload.php';

use Exception;
use Google_Service_Calendar;
use stewfun\common\Calendar as CommonCalendar;
use stewfun\common\Control;
use stewfun\facebook\EventICAL;
use stewfun\google\Calendar as GoogleCalendar;
use stewfun\wordpress\EventXML as WordpressEventXML;


/**
 * Set config into common class
 */
Control::$CONF = (require_once __DIR__ . '/config/config.php');
Control::$DIR  = __DIR__;

if (!isset(Control::$CONF['default_time_zone'])) {
    Control::$CONF['default_time_zone'] = 'Europe/Rome';
}


/**
 * Set some environment values
 */
date_default_timezone_set(Control::$CONF['default_time_zone']);


/**
 * Argument configuration
 */
$arguments = [
    ['', 'h', 'help', "Display this help"],
    [':', 'A', 'action', "[Mandatory] What the script should do (see \e[1mActions\e[0m below for a full description)"],
    ['', 'M', 'send-mail', "To send report as email attachment instead of to standard error (useful in cron). No mail without changes since last run"]
];

// Admitted values for action argument and relative description
$actions = [
    'check:install               ' => 'Verifies if the installation of the program and its dependencies has been done correctly',
    'listcal:google              ' => 'Lists all the calendars available on the Google account to which you have been granted access',
    'listevents:google-wordpress ' => 'Lists (print_r) all events from now to a year that are currently on the Google Calendar updated from Facebook',
    'listevents:google-facebook  ' => 'Lists (print_r) all events from now to a year that are currently on the Google Calendar updated from Wordpress',
    'listevents:wordpress        ' => 'Lists (print_r) all events in the Wordpress export files (exports needs to be performed manually and saved in data/ dir)',
    'listevents:facebook         ' => 'Lists (print_r) all events in the Facebook feed (Facebook feed is read automatically)',
    'dosync:wordpress            ' => 'Reads all events in the Wordpress export files and updates the selected Google Calendar',
    'dosync:facebook             ' => 'Downloads automatically events from Facebook feed and updates the selected Google Calendar',
    'test                        ' => 'For testing',
];

$available_actions = array_map('trim', array_keys($actions));

// Get passed options
Control::$OPTS = myGetopt($arguments);

// Check for help or mandatory options set
if (!is_array(Control::$OPTS) || empty(Control::$OPTS) ||
    (!isset(Control::$OPTS['h']) && !isset(Control::$OPTS['help']) &&
        !isset(Control::$OPTS['A']) && !isset(Control::$OPTS['action']))) {

    file_put_contents('php://stderr', "FATAL! Mandatory arguments missing, call 'php " . basename($argv[0]) . " -h|--help' for more informations.\n");
    exit(EXIT_ERR_BAD_PARAMS);
}

$displayHelp = false;
// Help was choosen (any other option is ignored, the script will display help page and exit)
if (isset(Control::$OPTS['h']) || isset(Control::$OPTS['help'])) {
    $displayHelp = true;
} // No help requested, check for mandatory options
else {
    $allMandatories = true;

    // action choosed via short option : check if it is allowed
    if (isset(Control::$OPTS['A']) && is_string(Control::$OPTS['A']) && in_array(Control::$OPTS['A'], $available_actions)) {
        $action = Control::$OPTS['A'];
    } // action choosed via long option : check if it is allowed
    elseif (isset(Control::$OPTS['action']) && is_string(Control::$OPTS['action']) && in_array(Control::$OPTS['action'], $available_actions)) {
        $action = Control::$OPTS['action'];
    } else {
        $allMandatories = false;
    }

    if (isset($action)) {
        $actionParts = explode(":", $action);

        Control::$action      = $action;
        Control::$actionParts = $actionParts;
    }

    if ($allMandatories == false) {
        file_put_contents('php://stderr', "FATAL! Too few arguments or with wrong values, call 'php " .
            basename($argv[0]) . " help' for more informations.\n");
        exit(EXIT_ERR_BAD_PARAMS);
    }
}


/**
 * HELP PAGE
 */
if ($displayHelp == true) {
    $help_message = <<< EOD
\e[1m\e[4mGOOGLE CALENDAR SYNCER\e[0m
This script performs a one-way sync from different sources to your Google Calendar(s).

\e[1m\e[4mAvailable syncs\e[0m
WORDPRESS EVENTS    ==>> GOOGLE CALENDAR
FACEBOOK ICAL FEED  ==>> GOOGLE CALENDAR

WORDPRESS-SYNC needs you manually export events and venues from Wordpress admin panel as xml files and save them
into the data/ folder, the events' xml-file should be renamed to stewfun-wordpress-wp-events.xml,
the venues' xml-file to stewfun-wordpress-wp-venues.xml.
Events on Wordpress should be managed with the plugin "The Events Calendar" (https://wordpress.org/plugins/the-events-calendar/).
You can automate events export with `wp` cli: https://developer.wordpress.org/cli/commands/export/.

FACEBOOK-SYNC is performed automatically, you need to set into the config/config.php file the url of
your Facebook ical feed. More details are in the config file.

\e[1m\e[4mInstallation and configuration\e[0m
The script uses composer for the dependencies and Google Calendar API to interact with Google Calendar.

\e[1m\e[4mGoogle Calendars\e[0m
This script can update one or more Google Calendar that belongs to the same Google account.
The script is authorized to manage Google calendars via OAUTH protocol, credentials are stored into config/credentials.json file.
If you need to change account, delete the file credentials.json and repeat the OAUTH procedure.
The OAUTH procedure starts automatically when you run the script after credentials.json removal.
Google calendars ids should be set in the config/config.php.

See README.md for all informations.

\e[1m\e[4mOutput\e[0m
Error and status messages are written to standard error, data informations to standard output or sent via mail (with -M|--send-mail option set).
The script set the exit code number, see the "EXIT ERROR CODES" section in the beginning of the script.

\e[1m\e[4mUsage\e[0m
EOD;

    $help_message .= "\n";
    $help_message .= "php " . $argv[0] . " [OPTIONS]\n\n";

    $help_message .= "\e[1m\e[4mOptions\e[0m\n";

    foreach ($arguments as $v) {
        $argSwitch = '';
        if ($v[1] != '') {
            $argSwitch .= '-' . $v[1];
            $argSwitch .= ($v[0] == ':' ? ' <VALUE>' : ($v[0] == '::' ? '[VALUE]' : ''));
        }
        if ($v[2] != '') {
            $argSwitch .= ($argSwitch != '' ? ', ' : '');
            $argSwitch .= '--' . $v[2];
            $argSwitch .= ($v[0] == ':' ? '=<VALUE>' : ($v[0] == '::' ? '[=<VALUE>]' : ''));
        }

        $help_message .= "  \e[1m" . $argSwitch . "\e[0m\n      " . $v[3] . "\n\n";
    }

    $help_message .= "\e[1m\e[4mActions\e[0m\n";

    foreach ($actions as $k => $v) {
        $help_message .= "  " . $k . ': ' . $v . "\n";
    }

    $help_message .= "\n\e[1m\e[4mVERSION:\e[0m " . SCRIPT_VERSION_SEMVER . ' - ' . SCRIPT_VERSION_DATE . "\n";

    file_put_contents('php://stdout', $help_message . "\n");
    exit(EXIT_OK);
}


/**
 * ACTION check:install : full install check
 */
if ($action == 'check:install') {

    __checkInstall('FULL');

    file_put_contents('php://stderr', 'SUCCESS! Script is installed successfully' . "\n");
    exit(EXIT_OK);
}


/**
 * Before to go, perform a full installation check
 */
__checkInstall('FULL');


/**
 * Check selected calendars
 */
$selectedCalendars = [];
foreach (Control::$CONF['google_calendars'] as $cal_target => $cal_id) {
    if (trim($cal_id) == '') {
        continue;
    }

    if (!isset($selectedCalendars[$cal_id])) {
        $selectedCalendars[$cal_id] = [];
    }
    $selectedCalendars[$cal_id][] = $cal_target;

    if (count($selectedCalendars[$cal_id]) > 1) {
        Control::sendMessage(date('Y-m-d H:i:s', time()) .
            " FATAL! The same Google Calendar is selected for different sources, check config/config.php\n",
            EXIT_ERR_BAD_CONFIGURATION);
    }
}


/**
 * ACTION listevents:wordpress or dosync:wordpress : get wordpress events by parsing XML files
 */
if ($action == 'listevents:wordpress' || $action == 'dosync:wordpress') {
    try {
        $wpXML = new WordpressEventXML(
            __DIR__ . '/data/stewfun-wordpress-wp-events.xml',
            __DIR__ . '/data/stewfun-wordpress-wp-venues.xml',
            [
                'ignore_provided_timezone' => true
            ]);
    } catch (Exception $e) {
        Control::sendMessage(date('Y-m-d H:i:s', time()) .
            " FATAL! Cannot instantiate Wordpress Events parser: " . $e->getMessage() . "\n",
            EXIT_ERR_XML_INIT);
    }

    $wpEvents = $wpXML->fetchAllAssoc();

    // Print event list and exit
    if ($action == 'listevents:wordpress') {
        Control::sendMessage(print_r($wpEvents, TRUE), EXIT_OK);
    } // Sync is requested
    else {
        // No need to continue if nothing is changed
        if ($wpXML->getSourceHasChanged() == false) {
            // Send-email mode: no mail!
            // so the script can be executed often without sending a lot of emails without changes
            // and consuming the quota of Google API account
            if (isset(Control::$OPTS['M']) || isset(Control::$OPTS['send-mail'])) {
                exit(EXIT_OK);
            }

            Control::sendMessage(date('Y-m-d H:i:s', time()) .
                " NOTICE! There have been no changes since the last time\n",
                EXIT_OK);
        }
    }
}


/**
 * ACTION listevents:facebook or dosync:facebook : get facebook events by parsing ICAL feed
 */
if ($action == 'listevents:facebook' || $action == 'dosync:facebook') {
    try {
        $fbICAL = new EventICAL(Control::$CONF['facebook_ical_feed'], __DIR__ . '/data/stewfun-facebook-fb-events.ics', [
            'defaultSpan'                 => 2,     // Default value
            'defaultTimeZone'             => 'UTC',
            'defaultWeekStart'            => 'MO',  // Default value
            'disableCharacterReplacement' => false, // Default value
            'skipRecurrence'              => false, // Default value
            'useTimeZoneWithRRules'       => false, // Default value
        ], [
            'timeZone' => Control::$CONF['default_time_zone']
        ]);
    } catch (Exception $e) {
        Control::sendMessage(date('Y-m-d H:i:s', time()) .
            " FATAL! Cannot istantiate Facebook ICAL parser: " . $e->getMessage() . "\n",
            EXIT_ERR_ICAL_INIT);
    }

    $fbEvents = $fbICAL->fetchAllAssoc();

    // Print event list and exit
    if ($action == 'listevents:facebook') {
        Control::sendMessage(print_r($fbEvents, TRUE), EXIT_OK);
    } // Sync is requested
    else {
        // No need to continue if nothing is changed
        if ($fbICAL->getSourceHasChanged() == false) {
            // Send-email mode: no mail!
            // so the script can be executed often without sending a lot of emails without changes
            // and consuming the quota of Google API account
            if (isset(Control::$OPTS['M']) || isset(Control::$OPTS['send-mail'])) {
                exit(EXIT_OK);
            }

            Control::sendMessage(date('Y-m-d H:i:s', time()) .
                " NOTICE! There have been no changes since the last time\n",
                EXIT_OK);
        }
    }
}


/**
 * Google Calendar instance
 */
try {
    $ggCal = new GoogleCalendar('Google Calendar Sync', Google_Service_Calendar::CALENDAR, [
        'client_secret' => __DIR__ . '/config/client_secret.json',
        'credentials'   => __DIR__ . '/config/credentials.json'
    ]);
} catch (Exception $e) {
    Control::sendMessage(date('Y-m-d H:i:s', time()) .
        " FATAL! Cannot instantiate Google Calendar API Client:" . $e->getMessage() . "\n",
        EXIT_ERR_GOOGLE_INIT);
}


/**
 * ACTION listcal:google : only google calendar listing
 */
if ($action == 'listcal:google') {
    $calendarList = $ggCal->getCalendarList();

    if (!is_array($calendarList) || empty($calendarList)) {
        Control::sendMessage("NOTICE! No calendars found on this Google Account\n", EXIT_OK);
    }

    $atLeastOneSelected = false;
    foreach ($calendarList as $calendar) {
        $isSelected = '';
        if (isset($selectedCalendars[$calendar->getId()])) {
            $atLeastOneSelected = true;
            $isSelected         = ' !!! SELECTED for ' . implode(", ", $selectedCalendars[$calendar->getId()]) . ' !!!';
        }

        Control::sendMessage(sprintf("SUMMARY: %s%s\n" .
            "ID     : %s\n" .
            "DESCR  : %s\n\n",
            $calendar->getSummary(),
            $isSelected,
            $calendar->getId(),
            $calendar->getDescription()), NULL);
    }

    if ($atLeastOneSelected == false) {
        Control::sendMessage("NOTICE! No calendar selected!\n", NULL);
    }

    Control::sendMessage("", EXIT_OK);
}


/**
 * ACTION listevents:google-* or dosync:* : get events of the selected google calendar
 */
if ($action == 'listevents:google-wordpress' || $action == 'listevents:google-facebook' ||
    $action == 'dosync:wordpress' || $action == 'dosync:facebook') {

    $extCalendarKey = '';
    if ($action == 'listevents:google-wordpress' || $action == 'dosync:wordpress') {
        $extCalendarKey = 'wordpress';
    }
    if ($action == 'listevents:google-facebook' || $action == 'dosync:facebook') {
        $extCalendarKey = 'facebook';
    }

    if (empty(Control::$CONF['google_calendars'][$extCalendarKey])) {
        Control::sendMessage(date('Y-m-d H:i:s', time()) .
            " FATAL! No Google Calendar for " . $extCalendarKey . " selected, check config/config.php!\n",
            EXIT_ERR_BAD_CONFIGURATION);
    }

    try {
        $googleEvents = $ggCal->getCalendarEventList(Control::$CONF['google_calendars'][$extCalendarKey], [
            'timeMin' => CommonCalendar::timeToIso8601('now'),
            'timeMax' => CommonCalendar::timeToIso8601('now + 1year')
        ], 'CalendarEventListToArrayByICalUID');
    } catch (Exception $e) {
        Control::sendMessage(date('Y-m-d H:i:s', time()) .
            " FATAL! Cannot retrieve events of the Google Calendar selected for " . $extCalendarKey . " : " . $e->getMessage() . "\n",
            EXIT_ERR_GOOGLE_GET_CAL_EVENTS);
    }

    // Print event list and exit
    if (substr($action, 0, 10) == 'listevents') {

        if (empty($googleEvents)) {
            Control::sendMessage(date('Y-m-d H:i:s', time()) .
                " NOTICE! No events in the Google Calendar selected for " . $extCalendarKey . ".\n",
                EXIT_OK);
        }

        Control::sendMessage(print_r($googleEvents, TRUE), EXIT_OK);
    }
}


/**
 * ACTION dosync:wordpress : perform the sync WORDPRESS==>>GOOGLE
 */
if ($action == 'dosync:wordpress') {

    $counters = [
        'wp_events_tot'              => count($wpEvents['events']),
        'google_events_tot'          => count($googleEvents),
        'inserted_events'            => 0,
        'updated_events'             => 0,
        'same_events'                => 0,
        'resumed_events'             => 0,
        'soft_cancelled_events'      => 0,
        'inserted_events_list'       => [],
        'updated_events_list'        => [],
        'same_events_list'           => [],
        'resumed_events_list'        => [],
        'soft_cancelled_events_list' => [],
    ];

    Control::sendMessage(date('Y-m-d H:i:s', time()) . "\t" .
        sprintf("INFO!\tPerforming sync\tTotal events in Wordpress export:\t%d\tActual events on selected Google Calendar:\t%d\n",
            $counters['wp_events_tot'],
            $counters['google_events_tot']),
        NULL);


    /**
     * Check WORDPRESS events
     */
    foreach ($wpEvents['events'] as $iCalUID => $wp_event_data) {

        // If this event is not present in the google event list try to search it directly via iCalUID
        // it can be an existing event set deleted and/or outside the default search time-range
        if (!isset($googleEvents[$iCalUID])) {

            try {
                $findEvent = $ggCal->getCalendarEventList(Control::$CONF['google_calendars']['wordpress'], [
                    'iCalUID'               => $iCalUID,
                    'showDeleted'           => true,
                    'showHiddenInvitations' => true,
                ], 'CalendarEventListToArrayByICalUID');
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot retrieve an event by iCalUID: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_GET_CAL_EVENTS);
            }

            // Store the event into the array
            if (is_array($findEvent) && isset($findEvent[$iCalUID])) {
                $googleEvents[$iCalUID] = $findEvent[$iCalUID];

                $counters['google_events_tot'] = count($googleEvents);
            }
        }


        // start_ts 'YYYY-MM-DD' full day
        if (strlen($wp_event_data['start_ts']) == 10) {
            $start_ts = [
                'date'     => $wp_event_data['start_ts'],
                'timeZone' => $wp_event_data['start_timezone']
            ];
        } // start_ts Normal TS
        else {
            $start_ts = [
                'dateTime' => $wp_event_data['start_ts'],
                'timeZone' => $wp_event_data['start_timezone']
            ];
        }

        // end_ts 'YYYY-MM-DD' full day
        if (strlen($wp_event_data['end_ts']) == 10) {
            $end_ts = [
                'date'     => $wp_event_data['end_ts'],
                'timeZone' => $wp_event_data['end_timezone']
            ];
        } // end_ts Normal TS
        else {
            $end_ts = [
                'dateTime' => $wp_event_data['end_ts'],
                'timeZone' => $wp_event_data['end_timezone']
            ];
        }


        // Prepare data array
        $event_data_for_google = [
            'iCalUID'     => $wp_event_data['iCalUID'],
            'status'      => $wp_event_data['status'] == 'publish' ? 'confirmed' : 'tentative',
            'summary'     => $wp_event_data['summary'],
            'location'    => $wp_event_data['location'],
            'start'       => $start_ts,
            'end'         => $end_ts,
            'description' => $wp_event_data['description']
        ];


        /**
         * present in WORDPRESS and not in GOOGLE : INSERT
         */
        if (!isset($googleEvents[$iCalUID])) {

            try {
                $google_event = $ggCal->insertCalendarEvent(Control::$CONF['google_calendars']['wordpress'], $event_data_for_google);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot insert event: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_INSERT_CAL_EVENT);
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tCREATED\t%s\t%s\t%s\t%s\n",
                $wp_event_data['id'],
                $wp_event_data['slug'],
                $wp_event_data['start_ts'],
                $google_event->getHtmlLink()), NULL);

            $counters['inserted_events']++;
            $counters['inserted_events_list'][] = [
                'summary'          => $wp_event_data['summary'],
                'start_ts'         => $wp_event_data['start_ts'],
                'remote_html_link' => $wp_event_data['html_link'],
                'google_html_link' => $google_event->getHtmlLink()
            ];
        } /**
         * present in WORDPRESS and in GOOGLE and different hash or status : UPDATE | RESUME
         */
        elseif (isset($googleEvents[$iCalUID]) &&
            ($googleEvents[$iCalUID]['hash'] != $wp_event_data['hash'] ||
                $googleEvents[$iCalUID]['status'] != $event_data_for_google['status'])) {

            try {
                $google_event = $ggCal->updateCalendarEvent(Control::$CONF['google_calendars']['wordpress'],
                    $googleEvents[$iCalUID]['id'], $event_data_for_google);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot update event: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_UPDATE_CAL_EVENT);
            }

            if ($googleEvents[$iCalUID]['hash'] != $wp_event_data['hash']) {
                $updateDesc = 'UPDATED';
                $counters['updated_events']++;
                $counters['updated_events_list'][] = [
                    'summary'          => $wp_event_data['summary'],
                    'start_ts'         => $wp_event_data['start_ts'],
                    'remote_html_link' => $wp_event_data['html_link'],
                    'google_html_link' => $google_event->getHtmlLink()
                ];
            } elseif ($googleEvents[$iCalUID]['status'] != $event_data_for_google['status']) {
                $updateDesc = 'RESUMED';
                $counters['resumed_events']++;
                $counters['resumed_events_list'][] = [
                    'summary'          => $wp_event_data['summary'],
                    'start_ts'         => $wp_event_data['start_ts'],
                    'remote_html_link' => $wp_event_data['html_link'],
                    'google_html_link' => $google_event->getHtmlLink()
                ];
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\t" . $updateDesc . "\t%s\t%s\t%s\t%s\n",
                $wp_event_data['id'],
                $wp_event_data['slug'],
                $wp_event_data['start_ts'],
                $google_event->getHtmlLink()), NULL);
        } /**
         * present in WORDPRESS and in GOOGLE and same hash : SAME
         */
        elseif (isset($googleEvents[$iCalUID]) &&
            $googleEvents[$iCalUID]['hash'] == $wp_event_data['hash']) {

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tUNCHANGED\t%s\t%s\t%s\t%s\n",
                $wp_event_data['id'],
                $wp_event_data['slug'],
                $wp_event_data['start_ts'],
                $googleEvents[$iCalUID]['html_link']), NULL);

            $counters['same_events']++;
            $counters['same_events_list'][] = [
                'summary'          => $wp_event_data['summary'],
                'start_ts'         => $wp_event_data['start_ts'],
                'remote_html_link' => $wp_event_data['html_link'],
                'google_html_link' => $googleEvents[$iCalUID]['html_link']
            ];
        }
    }


    /**
     * Check GOOGLE events
     */
    foreach ($googleEvents as $iCalUID => $gg_event_data) {

        /**
         * present in GOOGLE and not in WORDPRESS and status not 'cancelled' : UPDATE to status canceled (softdelete)
         */
        if (!isset($wpEvents['events'][$iCalUID]) && $gg_event_data['status'] != 'cancelled') {

            try {
                $google_event =
                    $ggCal->updateCalendarEvent(Control::$CONF['google_calendars']['wordpress'],
                        $gg_event_data['id'], ['status' => 'cancelled']);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot set event 'cancelled': " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_UPDATE_CAL_EVENT);
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tSOFTDELETED\t%s\t\t\t%s\n",
                $google_event->getICalUID(),
                $google_event->getHtmlLink()), NULL);

            $counters['soft_cancelled_events']++;
            $counters['soft_cancelled_events_list'][] = [
                'summary'          => $gg_event_data['summary'],
                'start_ts'         => $gg_event_data['start_ts'],
                'remote_html_link' => NULL,
                'google_html_link' => $google_event->getHtmlLink()
            ];
        }
    }


    /**
     * Final report
     */
    $rep_counters = [];
    foreach ($counters as $k => $v) {
        if (!is_scalar($v)) {
            continue;
        }
        $rep_counters[] = $k . '=' . $v;
    }

    Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
            "\tDONE!\t%s\n", implode("\t", $rep_counters)) . "\n", EXIT_OK,
        $counters, $wpXML->getFileSources());
}


/**
 * ACTION dosync:facebook : perform the sync FACEBOOK==>>GOOGLE
 */
if ($action == 'dosync:facebook') {

    $counters = [
        'fb_events_tot'              => count($fbEvents['events']),
        'google_events_tot'          => count($googleEvents),
        'inserted_events'            => 0,
        'updated_events'             => 0,
        'same_events'                => 0,
        'resumed_events'             => 0,
        'soft_cancelled_events'      => 0,
        'inserted_events_list'       => [],
        'updated_events_list'        => [],
        'same_events_list'           => [],
        'resumed_events_list'        => [],
        'soft_cancelled_events_list' => [],
    ];

    Control::sendMessage(date('Y-m-d H:i:s', time()) . "\t" .
        sprintf("INFO!\tPerforming sync\tTotal events in Facebook export:\t%d\tActual events on selected Google Calendar:\t%d\n",
            $counters['fb_events_tot'],
            $counters['google_events_tot']), NULL);


    /**
     * Check FACEBOOK events
     */
    foreach ($fbEvents['events'] as $iCalUID => $fb_event_data) {

        // If this event is not present in the google event list try to search it directly via iCalUID
        // it can be an existing event set deleted and/or outside the default search time-range
        if (!isset($googleEvents[$iCalUID])) {

            try {
                $findEvent = $ggCal->getCalendarEventList(Control::$CONF['google_calendars']['facebook'], [
                    'iCalUID'               => $iCalUID,
                    'showDeleted'           => true,
                    'showHiddenInvitations' => true,
                ], 'CalendarEventListToArrayByICalUID');
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot retrieve an event by iCalUID: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_GET_CAL_EVENTS);
            }

            // Store the event into the array
            if (is_array($findEvent) && isset($findEvent[$iCalUID])) {
                $googleEvents[$iCalUID] = $findEvent[$iCalUID];

                $counters['google_events_tot'] = count($googleEvents);
            }
        }


        // start_ts 'YYYY-MM-DD' full day
        if (strlen($fb_event_data['start_ts']) == 10) {
            $start_ts = [
                'date'     => $fb_event_data['start_ts'],
                'timeZone' => $fb_event_data['start_timezone']
            ];
        } // start_ts Normal TS
        else {
            $start_ts = [
                'dateTime' => $fb_event_data['start_ts'],
                'timeZone' => $fb_event_data['start_timezone']
            ];
        }

        // end_ts 'YYYY-MM-DD' full day
        if (strlen($fb_event_data['end_ts']) == 10) {
            $end_ts = [
                'date'     => $fb_event_data['end_ts'],
                'timeZone' => $fb_event_data['end_timezone']
            ];
        } // end_ts Normal TS
        else {
            $end_ts = [
                'dateTime' => $fb_event_data['end_ts'],
                'timeZone' => $fb_event_data['end_timezone']
            ];
        }


        // Prepare data array
        list($dummy, $partStatus, $dummy) = explode(":", $fb_event_data['status']);
        $event_data_for_google = [
            'iCalUID'     => $fb_event_data['id'],
            'status'      => strtolower(trim($partStatus)) == 'accepted' ? 'confirmed' : 'tentative',
            'summary'     => $fb_event_data['summary'],
            'location'    => $fb_event_data['location'],
            'start'       => $start_ts,
            'end'         => $end_ts,
            'description' => $fb_event_data['description']
        ];


        /**
         * present in FACEBOOK and not in GOOGLE : INSERT
         */
        if (!isset($googleEvents[$iCalUID])) {

            try {
                $google_event = $ggCal->insertCalendarEvent(Control::$CONF['google_calendars']['facebook'], $event_data_for_google);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot insert event: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_INSERT_CAL_EVENT);
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tCREATED\t%s\t%s\t%s\t%s\n",
                $fb_event_data['id'],
                $fb_event_data['slug'],
                $fb_event_data['start_ts'],
                $google_event->getHtmlLink()), NULL);

            $counters['inserted_events']++;
            $counters['inserted_events_list'][] = [
                'summary'          => $fb_event_data['summary'],
                'start_ts'         => $fb_event_data['start_ts'],
                'remote_html_link' => $fb_event_data['html_link'],
                'google_html_link' => $google_event->getHtmlLink()
            ];
        } /**
         * present in FACEBOOK and in GOOGLE and different hash or status : UPDATE | RESUME
         */
        elseif (isset($googleEvents[$iCalUID]) &&
            ($googleEvents[$iCalUID]['hash'] != $fb_event_data['hash'] ||
                $googleEvents[$iCalUID]['status'] != $event_data_for_google['status'])) {

            try {
                $google_event = $ggCal->updateCalendarEvent(Control::$CONF['google_calendars']['facebook'],
                    $googleEvents[$iCalUID]['id'], $event_data_for_google);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot update event: " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_UPDATE_CAL_EVENT);
            }

            if ($googleEvents[$iCalUID]['hash'] != $fb_event_data['hash']) {
                $updateDesc = 'UPDATED';
                $counters['updated_events']++;
                $counters['updated_events_list'][] = [
                    'summary'          => $fb_event_data['summary'],
                    'start_ts'         => $fb_event_data['start_ts'],
                    'remote_html_link' => $fb_event_data['html_link'],
                    'google_html_link' => $google_event->getHtmlLink()
                ];
            } elseif ($googleEvents[$iCalUID]['status'] != $event_data_for_google['status']) {
                $updateDesc = 'RESUMED';
                $counters['resumed_events']++;
                $counters['resumed_events_list'][] = [
                    'summary'          => $fb_event_data['summary'],
                    'start_ts'         => $fb_event_data['start_ts'],
                    'remote_html_link' => $fb_event_data['html_link'],
                    'google_html_link' => $google_event->getHtmlLink()
                ];
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\t" . $updateDesc . "\t%s\t%s\t%s\t%s\n",
                $fb_event_data['id'],
                $fb_event_data['slug'],
                $fb_event_data['start_ts'],
                $google_event->getHtmlLink()), NULL);
        } /**
         * present in FACEBOOK and in GOOGLE and same hash : SAME
         */
        elseif (isset($googleEvents[$iCalUID]) &&
            $googleEvents[$iCalUID]['hash'] == $fb_event_data['hash']) {

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tUNCHANGED\t%s\t%s\t%s\t%s\n",
                $fb_event_data['id'],
                $fb_event_data['slug'],
                $fb_event_data['start_ts'],
                $googleEvents[$iCalUID]['html_link']), NULL);

            $counters['same_events']++;
            $counters['same_events_list'][] = [
                'summary'          => $fb_event_data['summary'],
                'start_ts'         => $fb_event_data['start_ts'],
                'remote_html_link' => $fb_event_data['html_link'],
                'google_html_link' => $googleEvents[$iCalUID]['html_link']
            ];
        }
    }


    /**
     * Check GOOGLE events
     */
    foreach ($googleEvents as $iCalUID => $gg_event_data) {

        /**
         * present in GOOGLE and not 'cancelled' and not already finished and not in FACEBOOK :
         * UPDATE to status canceled (softdelete)
         *
         * we check if it is not finished because finished events may be no more in FACEBOOK export but still valid
         */
        if (!isset($fbEvents['events'][$iCalUID]) &&
            $gg_event_data['status'] != 'cancelled' &&
            $gg_event_data['end_ts'] > CommonCalendar::timeToIso8601('now')) {

            try {
                $google_event =
                    $ggCal->updateCalendarEvent(Control::$CONF['google_calendars']['facebook'],
                        $gg_event_data['id'], ['status' => 'cancelled']);
            } catch (Exception $e) {
                Control::sendMessage(date('Y-m-d H:i:s', time()) .
                    " FATAL! Cannot set event 'cancelled': " . $e->getMessage() . "\n",
                    EXIT_ERR_GOOGLE_UPDATE_CAL_EVENT);
            }

            Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
                "\tINFO!\tSOFTDELETED\t%s\t\t\t%s\n",
                $google_event->getICalUID(),
                $google_event->getHtmlLink()), NULL);

            $counters['soft_cancelled_events']++;
            $counters['soft_cancelled_events_list'][] = [
                'summary'          => $gg_event_data['summary'],
                'start_ts'         => $gg_event_data['start_ts'],
                'remote_html_link' => NULL,
                'google_html_link' => $google_event->getHtmlLink()
            ];
        }
    }


    /**
     * Final report
     */
    $rep_counters = [];
    foreach ($counters as $k => $v) {
        if (!is_scalar($v)) {
            continue;
        }
        $rep_counters[] = $k . '=' . $v;
    }

    Control::sendMessage(sprintf(date('Y-m-d H:i:s', time()) .
            "\tDONE!\t%s\n", implode("\t", $rep_counters)) . "\n", EXIT_OK,
        $counters, $fbICAL->getFileSources());
}


/**
 * Switch for testing purposes
 */
if ($action == 'test') {

    // ... put here code to test something ...

    exit(EXIT_OK);
}


/**
 * --------- Functions ---------
 */
/**
 * Test if the installation has been fully executed
 *
 * @param string $check_type
 *
 * @return bool|void
 */
function __checkInstall($check_type = 'BASIC')
{

    $client_secret   = __DIR__ . '/config/client_secret.json';
    $config_file     = __DIR__ . '/config/config.php';
    $credentials     = __DIR__ . '/config/credentials.json';
    $vendor_dir      = __DIR__ . '/vendor';
    $vendor_autoload = __DIR__ . '/vendor/autoload.php';
    $composer_lock   = __DIR__ . '/composer.lock';


    if (!is_dir($vendor_dir) || !is_file($vendor_autoload) || !is_readable($vendor_autoload) ||
        !is_file($composer_lock) || !is_readable($composer_lock) || !is_writable($composer_lock)) {
        file_put_contents('php://stderr', "FATAL: composer not executed or failed, see README.md to proper install dependencies with composer.\n");
        exit(EXIT_ERR_BAD_INSTALL);
    }

    if (!is_file($config_file) || !is_readable($config_file)) {
        file_put_contents('php://stderr', "FATAL: config/config.php is missing, see README.md to proper configure the script.\n");
        exit(EXIT_ERR_BAD_INSTALL);
    }

    if ($check_type == 'BASIC') {
        return;
    }

    if (!is_file($client_secret) || !is_readable($client_secret)) {
        file_put_contents('php://stderr', "FATAL: config/client_secret.json is missing, " .
            "see README.md to proper authorize the script to use the Google Calendar API.\n");
        exit(EXIT_ERR_BAD_INSTALL);
    }

    if (!is_file($credentials) || !is_readable($credentials)) {

        require __DIR__ . '/vendor/autoload.php';

        readline("NOTICE! You need to allow the script to manage the calendars of your Google Account, " .
            "press ENTER to continue and follow the instructions... ");

        file_put_contents('php://stderr', "\n");

        try {
            $ggCal = new GoogleCalendar('Google Calendar Sync', Google_Service_Calendar::CALENDAR, [
                'client_secret' => __DIR__ . '/config/client_secret.json',
                'credentials'   => __DIR__ . '/config/credentials.json'
            ]);
        } catch (Exception $e) {
            file_put_contents('php://stderr', "FATAL! " . $e->getMessage() . "\n");
            exit(EXIT_ERR_GOOGLE_INIT);
        }
    }

    if (isset(Control::$OPTS['M']) || isset(Control::$OPTS['send-mail'])) {

        if (!isset(Control::$CONF['email_report']) || !is_array(Control::$CONF['email_report']) ||
            !isset(Control::$CONF['email_report']['use'])) {

            file_put_contents('php://stderr', "FATAL! Email requested but not configured, check config.php\n");
            exit(EXIT_ERR_BAD_CONFIGURATION);
        }

        if (trim(Control::$CONF['email_report']['use']) == '') {

            file_put_contents('php://stderr', "FATAL! Email requested but not enabled, check config.php\n");
            exit(EXIT_ERR_BAD_CONFIGURATION);
        }
    }

    return TRUE;
}

/**
 * Fallback for PHP installations without readline support
 *
 * @param null $prompt
 *
 * @return string
 */
function readline($prompt = NULL)
{
    if (function_exists('\readline')) {
        return \readline($prompt);
    }

    if ($prompt) {
        echo $prompt;
    }
    $fp   = fopen("php://stdin", "r");
    $line = rtrim(fgets($fp, 1024));
    return $line;
}

/**
 * Returns the result of getopt()
 *
 * @param $arguments Array of configured options
 *
 * @return array
 */
function myGetopt($arguments)
{
    $shortOpts = '';
    $longOpts  = [];
    foreach ($arguments as $v) {
        $shortOpts .= $v[1] . $v[0];
        if ($v[2] != '') {
            $longOpts[] = $v[2] . $v[0];
        } else {
            $longOpts[] = '';
        }
    }
    return getopt($shortOpts, $longOpts);
}